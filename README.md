# Use-Case Raumklima-Monitoring

## Projektbeschreibung

In diesem Use-Case wurde die Integration der "DEOS SAM Raumluftampel" durchgeführt. Der Sensor misst die relative Luftfeuchtigkeit, Temperatur und CO2-Konzentration in einem Raum.

Der Use-Case wurde auf der FIWARE-basierten [Urban Dataspace Platform](https://gitlab.com/urban-dataspace-platform/core-platform) entwickelt und integriert.

## Installationsanleitung

Um den Anwendungsfall zu installieren sind folgende Schritte nötig:  
1. Eine lokale Maschine für die Installation ist vorzubereiten (Siehe https://gitlab.com/urban-dataspace-platform/core-platform/-/blob/master/00_documents/Admin-guides/cluster.md).
2. Eine NodeRED-Instanz deployen oder eine vorhandene Instanz verwenden:
    - Deployment analog zu den *demo_usecases*: https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/deployment
3. *flow.json* in der NodeRED-Instanz deployen.

## Gebrauchsanweisung

Der NodeRED-Flow ruft die Daten per HTTP-Request von der ElementIOT-API ab. Dabei kann eine Folder-ID angegeben werden, um alle Devices aus einem Ordner mit einem Request abzufragen. Für die ElementIOT-API ist ein API-Key erforderlich.

Diese Daten werden in das NGSI-kompatible [AirQualityObserved Smart-Data-Model](https://github.com/smart-data-models/dataModel.Environment/blob/master/AirQualityObserved/README.md) umgewandelt und an die Datenplattform gesendet:

```json
{
   "id":"urn:ngsi-ld:AirQualityObserved:ca150bd7-5ca2-4d5d-a21c-18c7d822aff3",
   "type":"AirQualityObserved",
   "name":{
      "type":"Text",
      "value":"0828_TFG_2OG_304_00000b49"
   },
   "alternateName":{
      "type":"Text",
      "value":"0828-tfg-2og-304-00000b49"
   },
   "dateObserved":{
      "type":"DateTime",
      "value":"2024-05-03T13:18:06.868Z"
   },
   "co2":{
      "type":"Number",
      "value":395
   },
   "relativeHumidity":{
      "type":"Number",
      "value":45
   },
   "temperature":{
      "type":"Number",
      "value":22.3
   },
   "location":{
      "type":"geo:json",
      "value":{
         "coordinates":[
            <<longitude>>,
            <<latitude>>
         ],
         "type":"Point"
      }
   }
}
```

## Kontaktinformationen

Dieses Projekt wurde von der [Hypertegrity AG](https://www.hypertegrity.de/) entwickelt.

## Lizenz

Dieses Projekt wird unter der EUPL 1.2 veröffentlicht.

## Link zum Original-Repository

https://gitlab.com/urban-dataspace-platform/use_cases/integration/raumklima_monitoring/nr-deos-sam-raumluftampel